// Lab6.cpp : Questo file contiene la funzione 'main', in cui inizia e termina l'esecuzione del programma.
//

#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <ObjectTrackerTest.h>

int main(int argc, char** argv)
{
    //std::string pathV = argv[1];
    //std::string dirImages = argv[2];
    std::string pathV = "../data/video.mov";
    std::string dirImages = "../data/objects/*.png";

    ObjectTrackerTest obj = ObjectTrackerTest(pathV, dirImages);
    obj.extractSIFTFeature();
    //obj.extractORBFeature();
    obj.matchSIFT();
    //obj.match();
}

