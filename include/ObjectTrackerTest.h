//io uso questi e mi funzione sift se vuoi provare ad utilizzare anche tu sift usando questi header puoi
#include <opencv2/opencv.hpp>
#include <iostream>
#include <opencv2/xfeatures2d.hpp>

class ObjectTrackerTest {

public:

	ObjectTrackerTest();
	ObjectTrackerTest(std::string, std::string);
	void match();
	void matchSIFT();
	void extractORBFeature();
	void extractSIFTFeature();



protected:

	std::vector<cv::Mat> imgs;
	std::vector<cv::Mat> frames;
	std::vector<cv::Mat> framesDescriptors, examplesDescriptors;
	std::vector<std::vector<cv::DMatch>> matches;
	std::vector<std::vector<cv::DMatch>> good_matches;		//nuovo vettore con tutti i matches senza outliners
	std::vector<std::vector<cv::KeyPoint>> framesKeypoints, examplesKeypoints;
};