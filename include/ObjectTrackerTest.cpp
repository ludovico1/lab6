//semplificato gli header e li ho spostati nel .h perch� mi piacciono di pi� la XD
#include <ObjectTrackerTest.h>
	
	//ho impostato cv come namespace cos� non misogna scrivere cv:: ognivolta
	using namespace cv;

	ObjectTrackerTest::ObjectTrackerTest()
	{}

	ObjectTrackerTest::ObjectTrackerTest(std::string videoPath, std::string photoDir)
	{
		std::vector<cv::String> filename;
		//vesione di glob diversa adesso non serve specificare hardcoded il tipo di immagini basta metterelo nel path tipo ../data/picture/*.jpg
		glob(photoDir, filename, true);
		std::cout << filename.size() << std::endl;
		for (int i = 0; i < filename.size(); i++)
		{
			imgs.push_back(cv::imread(filename[i]));
			std::cout << filename[i] << std::endl;
		}


		cv::VideoCapture cap(videoPath);
		cv::Mat frame;

		if (cap.isOpened())
		{
			for (;;)
			{
				cap.read(frame);

				if (!frame.empty())
					frames.push_back(frame);
				else 
				{
					std::cout << "All frames captured!";
					break;
				}
			}
		}
		else
		{
			std::cerr << "Error in opening video";
		}
	}

	//domanda perch� estrai framesKeypoints due volte don basta solo compute?
	//doanda numero 2 quand'� che calcoli i key points demme immagini? perche usi di nuovo frames
	void ObjectTrackerTest::extractORBFeature()
	{
		// Initiate ORB detector
		cv::Ptr<cv::FeatureDetector> detector = cv::ORB::create();
		// find the keypoints and descriptors with ORB of frames
		detector->detect(frames, framesKeypoints);
		cv::Ptr<cv::DescriptorExtractor> extractor = cv::ORB::create();
		extractor->compute(frames, framesKeypoints, framesDescriptors);

		cv::Ptr<cv::FeatureDetector> detector2 = cv::ORB::create();
		// find the keypoints and descriptors with ORB of example objects
		detector->detect(imgs, examplesKeypoints);
		cv::Ptr<cv::DescriptorExtractor> extractor2 = cv::ORB::create();
		extractor->compute(imgs, examplesKeypoints, examplesDescriptors);
	}



	void ObjectTrackerTest::match()
	{
		cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_HAMMING);

		int i = 0;
			std::vector<cv::DMatch> match, thresholded_match;
			
			float ratio = 4;

			for (int j = 0; j < imgs.size(); j++)
			{
				std::cout << "matching..." << std::endl;
				
				matcher->match(framesDescriptors[i], examplesDescriptors[j], match);
				float minD = match[0].distance;
				for (int k = 0; k < match.size(); k++)
					if (match[k].distance < minD)
						minD = match[k].distance;

				for (int k = 0; k < match.size(); k++)
					if (match[k].distance < minD * ratio)
						thresholded_match.push_back(match[k]);

				matches.push_back(thresholded_match);	

				/*cv::Mat tmp;
				cv::drawMatches(frames[i], framesKeypoints[i], imgs[j], examplesKeypoints[j], thresholded_match, tmp);*/

			}

		//sistemto remove outliners

		std::vector<cv::Mat> masks;

			for (int j = 0; j < imgs.size(); j++)
			{
				std::cout << "mask..." << std::endl;
				std::vector<cv::Point2f> source;
				std::vector<cv::Point2f> destination;

				for (int k = 0; k < matches.size(); k++)
				{
					source.push_back(framesKeypoints[i][matches[i * imgs.size() + j][k].queryIdx].pt);
					destination.push_back(examplesKeypoints[j][matches[i * imgs.size() + j][k].trainIdx].pt);
				}

				masks.push_back(cv::Mat());
				cv::Mat homography = cv::findHomography(source, destination, masks[i * imgs.size() + j], cv::RANSAC);
				std::vector <DMatch>tmp_good_matches;
				for (int k = 0; k < masks[i * imgs.size() + j].rows; k++)
					if (masks[i * imgs.size() + j].at<int>(k))
					{
						tmp_good_matches.push_back(matches[i * imgs.size() + j][k]);
					}
				good_matches.push_back(tmp_good_matches);
			}
			Mat tmp2;
		cv::waitKey();
		drawMatches(frames[0], framesKeypoints[0], imgs[1], examplesKeypoints[1], good_matches[1], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test2", WINDOW_NORMAL);
		imshow("test2", tmp2);
		cv::waitKey();

	}

	void ObjectTrackerTest::extractSIFTFeature()
	{
		// Initiate SIFT detector
		cv::Ptr<Feature2D> f2d = xfeatures2d::SIFT::create();
		// find the keypoints and descriptors with SIFT of frames
		std::vector<KeyPoint> tmpKeypoints;
		Mat tmpDescriptors;
		f2d->detectAndCompute(frames[0], Mat(), tmpKeypoints, tmpDescriptors);
		framesKeypoints.push_back(tmpKeypoints);
		framesDescriptors.push_back(tmpDescriptors);
		// find the keypoints and descriptors with SIFT of reference images
		for each (Mat img in imgs)
		{
			f2d->detectAndCompute(img, Mat(), tmpKeypoints, tmpDescriptors);
			examplesKeypoints.push_back(tmpKeypoints);
			examplesDescriptors.push_back(tmpDescriptors);
		}
		Mat output2;
		/*namedWindow("test3");
		drawKeypoints(imgs[0], examplesKeypoints[0], output2);
		imshow("test3", output2);
		/*namedWindow("test4");
		drawKeypoints(imgs[1], examplesKeypoints[1], output2);
		imshow("test4", output2);
		namedWindow("test5");
		drawKeypoints(imgs[2], examplesKeypoints[2], output2);
		imshow("test5", output2);
		namedWindow("test6");
		drawKeypoints(imgs[3], examplesKeypoints[3], output2);
		imshow("test6", output2);*/

	}

	//datp una sistemata al match per farlo andare con orb basta sostistuire norm nel create del matcher
	void ObjectTrackerTest::matchSIFT()
	{
		cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_L2);

		int i = 0;

		float ratio = 2;

		for (int j = 0; j < imgs.size(); j++)
		{
			std::vector<cv::DMatch> match, thresholded_match;
			std::cout << "matching..." << std::endl;

			matcher->match(framesDescriptors[i], examplesDescriptors[j], match);
			float minD = match[0].distance;
			for (int k = 0; k < match.size(); k++)
				if (match[k].distance < minD)
					minD = match[k].distance;

			for (int k = 0; k < match.size(); k++)
				if (match[k].distance < minD * ratio)
					thresholded_match.push_back(match[k]);

			matches.push_back(thresholded_match);

			/*cv::Mat tmp;
			cv::drawMatches(frames[i], framesKeypoints[i], imgs[j], examplesKeypoints[j], thresholded_match, tmp);*/

		}
		Mat tmp2;
		/*drawMatches(frames[0], framesKeypoints[0], imgs[1], examplesKeypoints[1], matches[1], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test3", WINDOW_NORMAL);
		imshow("test3", tmp2);*/
		//waitKey();

		//sistemto remove outliners

		//std::vector<cv::Mat> masks;

		for (int j = 0; j < imgs.size(); j++)
		{
			std::cout << "mask..." << std::endl;
			std::vector<cv::Point2f> source;
			std::vector<cv::Point2f> destination;

			for (int k = 0; k < matches[j].size(); k++)
			{
				source.push_back(framesKeypoints[i][matches[i * imgs.size() + j][k].queryIdx].pt);
				destination.push_back(examplesKeypoints[j][matches[i * imgs.size() + j][k].trainIdx].pt);
			}

			Mat mask;
			cv::Mat homography = cv::findHomography(source, destination, mask, cv::RANSAC);
			std::vector <DMatch>tmp_good_matches;
			std::cout << mask << std::endl;
			for (int k = 0; k < mask.rows; k++)
				if (mask.at<int>(k))
				{
					tmp_good_matches.push_back(matches[i * imgs.size() + j][k]);
				}
			good_matches.push_back(tmp_good_matches);
		}
		drawMatches(frames[0], framesKeypoints[0], imgs[0], examplesKeypoints[0], good_matches[0], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test", WINDOW_NORMAL);
		imshow("test", tmp2);
		drawMatches(frames[0], framesKeypoints[0], imgs[1], examplesKeypoints[1], good_matches[1], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test2", WINDOW_NORMAL);
		imshow("test2", tmp2);
		drawMatches(frames[0], framesKeypoints[0], imgs[2], examplesKeypoints[2], good_matches[2], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test3", WINDOW_NORMAL);
		imshow("test3", tmp2);
		drawMatches(frames[0], framesKeypoints[0], imgs[3], examplesKeypoints[3], good_matches[3], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test4", WINDOW_NORMAL);
		imshow("test4", tmp2);
		cv::waitKey();

	}



	/*void ObjectTrackerTest::matchSIFT()
	{
		cv::Ptr<cv::BFMatcher> matcher = cv::BFMatcher::create(cv::NORM_L2);

			
			float ratio = 4;
			cv::Mat tmp,tmp2;
			std::cout << "inizio match" << std::endl;
			for (int j = 0; j < imgs.size(); j++)
			{
				std::vector<cv::DMatch> match, thresholded_match;
				std::cout << "matching" << std::endl;
				matcher->match(framesDescriptors[0], examplesDescriptors[j], match);
				float minD = match[0].distance;
				std::cout << match.size() << std::endl;
				for each (DMatch var in match)
				{
					if (var.distance < minD)
					{
						minD = var.distance;
					}
				}
				std::cout << "fine min dist" << std::endl;
				for each (DMatch var in match)
				{
					if (var.distance <= ratio * minD)
					{
						thresholded_match.push_back(var);
					}
				}
				std::cout << "end matching" << std::endl;
				matches.push_back(thresholded_match);

				
				//drawMatches(tmp, framesKeypoints[0], imgs[j], examplesKeypoints[j], thresholded_match, tmp, Scalar(255, 0, 0),Scalar(255,0,0), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
				//cv::drawMatches(frames[0], framesKeypoints[0], imgs[j], examplesKeypoints[0], thresholded_match, tmp);
				
			}
			std::cout << examplesKeypoints.size() << std::endl;
			std::cout << "match size" << std::endl;
			std::cout << matches[1].size() << std::endl;
			std::cout << framesKeypoints.size() << std::endl;
			/*drawMatches(frames[0], framesKeypoints[0], imgs[1], examplesKeypoints[1], matches[1], tmp, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
			namedWindow("test", WINDOW_NORMAL);
			imshow("test", tmp);*/


		//sistemto remove outliners

		/*std::vector<cv::Mat> masks;
		int count = 0;
		for each (std::vector<DMatch> var in matches)
		{
			std::vector<Point2f> obj;
			std::vector<Point2f> scene;
			for (size_t i = 0; i < var.size(); i++)
			{
				//-- Get the keypoints from the good matches
				obj.push_back(framesKeypoints[0][var[i].queryIdx].pt);
				scene.push_back(examplesKeypoints[count][var[i].trainIdx].pt);
			}
			Mat mask;
			Mat H = findHomography(obj, scene, mask, RANSAC );
			masks.push_back(mask);
			std::vector <DMatch>tmp_good_matches;
			for (int k = 0; k < mask.rows; k++)
				if (mask.at<int>(k))
				{
					tmp_good_matches.push_back(matches[count][k]);
				}
			good_matches.push_back(tmp_good_matches);
			count++;
		}
		std::cout << "match size good" << std::endl;
		std::cout << good_matches[1].size() << std::endl;
		/*drawMatches(frames[0], framesKeypoints[0], imgs[1], examplesKeypoints[1], good_matches[1], tmp2, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		namedWindow("test2", WINDOW_NORMAL);
		imshow("test2", tmp2);*/
		//cv::waitKey();
	//}
